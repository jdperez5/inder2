package co.com.proyectobase.screenplay.tasks;


import co.com.proyectobase.screenplay.userinterface.InderHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task{

	private InderHomePage INDER;
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(INDER));
	}

	public static Abrir LaPaginaDeInder() {
		// TODO Auto-generated method stub
		return Tasks.instrumented(Abrir.class);
	}

}
