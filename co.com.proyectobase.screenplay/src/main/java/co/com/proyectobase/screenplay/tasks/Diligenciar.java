package co.com.proyectobase.screenplay.tasks;
 
import java.util.List;

import org.openqa.selenium.Keys;

import co.com.proyectobase.screenplay.model.DatosDelInder;
import static co.com.proyectobase.screenplay.userinterface.LlenarFormulario.*;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;

public class Diligenciar implements Task {

	private List<DatosDelInder> formulario;

	public Diligenciar(List<DatosDelInder> formulario) {
		super();
		this.formulario = formulario;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		//Se hace clic en tipo de persona, se sit�a en el buscador y se ingresa persona natural.
		actor.attemptsTo(Click.on(TIPO_DE_PERSONA));
		actor.attemptsTo(Enter.theValue(formulario.get(0).getTipopersona()).into(TIPO_DE_PERSONA_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(TIPO_DE_PERSONA_SEARCH));
		
		
		//Se hace clic en tipo de document�, se sit�a en el buscador y se ingresa cedula de ciudadan�a.
		actor.attemptsTo(Click.on(TIPO_DE_DDOCUMENTO));
		actor.attemptsTo(Enter.theValue(formulario.get(0).getTipodocumento()).into(TIPO_DE_DOCUMENTO_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(TIPO_DE_DOCUMENTO_SEARCH));
		
		
		//Se ingresa el número de documento 
		
		actor.attemptsTo(Enter.theValue(formulario.get(0).getNumeroidentificacion()).into(INGRESO_DE_DOCUMENTO));
		
		
		
		//Se ingresa el número de documento 
		
		actor.attemptsTo(Enter.theValue(formulario.get(0).nombres).into(INGRESO_DE_NOMBRE));
	
		//Se ingresa el Apellido 
		
		actor.attemptsTo(Enter.theValue(formulario.get(0).apellidos).into(INGRESO_DE_APELLIDO));
		
		//Se selecciona y escoge un sexo
		actor.attemptsTo(Click.on(SEXO));
		actor.attemptsTo(Enter.theValue(formulario.get(0).sexo).into(SEXO_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(SEXO_SEARCH));
		
		
		//Se ingresa la fecha de nacimiento
		
		actor.attemptsTo(Enter.theValue(formulario.get(0).fechanacimiento).into(FECHA_DE_NACIMIENTO));
		
		//Se ingresan las dos claves
		
		actor.attemptsTo(Enter.theValue(formulario.get(0).confclave).into(CONTRASENA_UNO));
		
		
		actor.attemptsTo(Enter.theValue(formulario.get(0).confclave).into(CONTRASENA_DOS));
		
	    //Se ingresa el municipio
		actor.attemptsTo(Click.on(MUNICIPIO));
		actor.attemptsTo(Enter.theValue(formulario.get(0).municipio).into(MUNICIPIO_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(MUNICIPIO_SEARCH));

		
		
		
		//Se ingresa el estrato
		actor.attemptsTo(Click.on(ESTRATO));
		actor.attemptsTo(Enter.theValue(formulario.get(0).estrato).into(ESTRATO_SEARCH));
		actor.attemptsTo(Hit.the(Keys.ENTER).into(ESTRATO_SEARCH));
		
		
		//se ingresa el correo electronico
		actor.attemptsTo(Enter.theValue(formulario.get(0).correo).into(CORREO));
		
		//se ingresa el telefono movil
		actor.attemptsTo(Enter.theValue(formulario.get(0).telefono).into(TELEFONO_MOVIL));
		
		//se aceptan los terminos y condiciones
		actor.attemptsTo(Click.on(ACEPTA_POLITICA));
		//se aceptan los terminos
		actor.attemptsTo(Click.on(ACEPTA_TERMINOS));
		//se guarda el formulario
		actor.attemptsTo(Click.on(GUARDA_EL_FORMULARIO));
	}

	public static Diligenciar laInscripcionCon(List<DatosDelInder> formulario) {
		return Tasks.instrumented(Diligenciar.class, formulario);

	}
}
