package co.com.proyectobase.screenplay.userinterface;




import org.openqa.selenium.By;

import net.serenitybdd.screenplay.targets.Target;



public class LlenarFormulario {

	
	public static final Target TIPO_DE_PERSONA = Target.the("Hacer click en tipo de persona")
			.located(By.id("select2-chosen-15"));

	public static final Target TIPO_DE_PERSONA_SEARCH = Target.the("Hacer click en el buscador de tipode persona")
			.located(By.id("s2id_autogen15_search"));
	
	
	public static final Target TIPO_DE_DDOCUMENTO = Target.the("Hacer click en tipo de documento")
			.located(By.id("select2-chosen-16"));
	
	
	public static final Target TIPO_DE_DOCUMENTO_SEARCH = Target.the("Hacer click en el buscador de tipode persona")
			.located(By.id("s2id_autogen16_search"));
	
	
	public static final Target INGRESO_DE_DOCUMENTO = Target.the("Se inserta el numero de documento")
			.located(By.id("numeroidentificacion"));
	
	
	public static final Target INGRESO_DE_NOMBRE = Target.the("Se inserta el nombre del usuario")
			.located(By.id("nombres"));
	
	public static final Target INGRESO_DE_APELLIDO = Target.the("Se inserta el apellido del usuario")
			.located(By.id("apellidos"));
	
	
	
	public static final Target SEXO= Target.the("Hacer click en la opción de seleccionar SEXO")
	.located(By.id("s2id_genero"));
	
	
	public static final Target SEXO_SEARCH = Target.the("Hacer click en el buscador SEXO")
			.located(By.id("s2id_autogen17_search"));
	
	
	public static final Target FECHA_DE_NACIMIENTO = Target.the("se ingresa la fecha de nacimiento")
			.located(By.id("fechanacimiento"));
	
	
	public static final Target CONTRASENA_UNO = Target.the("se ingresa la primer contraseña")
			.located(By.id("clave_uno"));
	
	
	public static final Target CONTRASENA_DOS = Target.the("se ingresa la segunda contraseña")
			.located(By.id("clave_dos"));
	
	
	public static final Target MUNICIPIO = Target.the("se ingresa el municipio")
			.located(By.id("s2id_municipio"));
	
	public static final Target MUNICIPIO_SEARCH = Target.the("se ingresa el barrio")
			.located(By.id("s2id_autogen19_search"));
	
	public static final Target BARRIO = Target.the("se ingresa la segunda contraseña")
			.located(By.id("select2-chosen-20"));
	
	public static final Target BARRIO_SEARCH = Target.the("se ingresa la segunda contraseña")
			.located(By.id("s2id_autogen20_search"));
	
	public static final Target OPCION_BARRIO= Target.the("se selecciona la opcion barrio")
			.located(By.id("formulario_registro_direccionOcomuna_1"));
	
	
	
	public static final Target DIRECCION = Target.the("se ingresa la direccion")
			.located(By.id("direccion"));
	
	
	
	
	public static final Target ESTRATO = Target.the("se ingresa la segunda contraseña")
			.located(By.id("s2id_formulario_registro_estrato"));
	
	public static final Target ESTRATO_SEARCH = Target.the("se ingresa la segunda contraseña")
			.located(By.id("s2id_autogen28_search"));
	
	public static final Target CORREO = Target.the("se ingresa la segunda contraseña")
			.located(By.id("correoelectronico"));
	
	public static final Target TELEFONO_MOVIL = Target.the("se ingresa la segunda contraseña")
			.located(By.id("telefonomovil"));
	
	
	public static final Target ACEPTA_POLITICA = Target.the("Check Habeas Data")
		            .located(By.className("icheckbox_square-blue"));
			
	public static final Target ACEPTA_TERMINOS = Target.the("Check to accept policies")
		           .located(By.xpath("//div[@class='infoBox row']//div[7]//div[1]")); 

	
	public static final Target GUARDA_EL_FORMULARIO = Target.the("se ingresa la segunda contraseña")
			.located(By.id("registro_save"));
	
	
	
}
