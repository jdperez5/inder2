package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.openqa.selenium.WebDriver;


import co.com.proyectobase.screenplay.model.DatosDelInder;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Diligenciar;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;


public class InderGoogleStepDefinition {
		
    @Managed(driver="chrome")
	private WebDriver hisBrowser;
    private Actor juan = Actor.named("Juan");
	
	@Before
	public void configuracionInicial(){
		juan.can(BrowseTheWeb.with(hisBrowser));
	}
	
		
	@Given("^que Juan quiere registrarse$")
	public void queJuanQuiereRegistrarse()  {
		juan.wasAbleTo(Abrir.LaPaginaDeInder());
		
	}
	
	@When("^el diligencia el Formulario$")
	public void elDiligenciaElFormulario(List<DatosDelInder> datosDelInder) throws Exception {
		juan.attemptsTo(Diligenciar.laInscripcionCon(datosDelInder));
	}
	
	
	@Then("^el verifica el mensaje de registro exitoso$")
	public void elVerificaElMensajeDeRegistroExitoso() throws Exception {

	}
		
	
	

}
