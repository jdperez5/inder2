package co.com.screenplay.Inder;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

//camelcase es el que ejecuta los steps definidos si no lo pones no es indispensable por que el lo pone por defecto
// un runer por proyecto es una buena practica
//git en vsts se crean las tareas se da en clonar en la esquina derecha y el nos da una ruta paraa copiar en nuestro computadpr
//click derecho click bash y adentro git clone
//para enviar documento al git le dmaos git add. .--all para subir todo , git status, git commit git commit - m "" para coentar el commit
//git push para envia r a vsts pull request para hacer a una rama principal
//pull request envia a archivos a una rama y combinelo la raiz se mira en bracnes
//git ignore hacer uno generico pero tamien agreguen unas lineas
//debo de hacer feedback de errores propios 
//
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src/test/resources/features/Inder.feature",
		tags= "@Registro",
		glue="co.com.proyectobase.screenplay.stepdefinitions",
		snippets=SnippetType.CAMELCASE
		
		)
public class RunnerTags {

	
	
}
